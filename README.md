## Example of work in progres linked to a web publication

The classic publication process can be an difficult way. Even more for young 
scientist which have to handle it with thousands and thousand publications around
the world and each year. 

This example is a way for sharing your work (can a published work or a work in 
progress - doesn't matter) quickly and maybe you can expect some actives feebacks
is you share it (can be sharing public or privat). 

When you are loged in gitlab, creat a new project. After you start that new project
you can sharing an co-working with other users on same script, data, file (same directory) 
with git. When you want to open it to more people you can serve/deploy more infomation 
with a webpage. Depend of your level in web-devlopping, you have the choice between 
different system (for more inforamtion visit [https://docs.gitlab.com/ce/ci/](https://docs.gitlab.com/ce/ci/)). 

Here, I chosed a small example with HTML5, CSS and Javascript (Math, MathJax26 and D3js).
